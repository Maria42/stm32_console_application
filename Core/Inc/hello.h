/*
 * hello.h
 *
 *  Created on: Sep 29, 2020
 *      Author: maria
 */

#ifndef INC_HELLO_H_
#define INC_HELLO_H_

#include "shell.h"
#include "FreeRTOS.h"
#include <stdio.h>

#define HELLO_CMD "hello"

void cmd_hello(int argc,char *argv[]);
#define HELLO_COMMANDS \
{ .command_name=HELLO_CMD, .function=cmd_hello}

BaseType_t hello_init();
BaseType_t hello_deinit();
void hello_function(const void *args);

#endif /* INC_HELLO_H_ */


