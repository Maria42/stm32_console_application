/*
 * hello.c
 *
 *  Created on: Sep 29, 2020
 *      Author: maria
 */
#include "hello.h"
#include "stdbool.h"
#include "string.h"
#include "cmsis_os.h"
#include "shell.h"

#define MIN_VALAUE 42
#define MAX_VALUE  420
#define CMD_ARG_SET "set" //defining a string in a macro
#define CMD_ARG_GET "get"
#define TASK_DELAY_MS 1000 //1000ms

static struct{
	 bool initialized;
	 uint16_t value;
}hello_control;

BaseType_t hello_init(){
    hello_control.initialized=true;
    hello_control.value=MIN_VALAUE;
    return pdTRUE; //same as return EXIT_SUCCESS in int/uint type functions
}

BaseType_t hello_deinit(){
    return pdTRUE;
}

void hello_function(const void *args){
     for(;;){
    	 hello_control.value=hello_control.value<MAX_VALUE
    		 ?hello_control.value+1
    	     :MIN_VALAUE;
    	 osDelay(TASK_DELAY_MS);
     }
}

static void cmd_usage(){
       sh_printf("usage: %s [set <value>] | get]\r\n",HELLO_CMD);
}

void cmd_hello(int argc,char *argv[]){
     if(!hello_control.initialized){
    	sh_printf("hello is not initialized yet\rn");
    	return;
     }

     if(argc<1){
		   cmd_usage();
		   return;
     }

     if(strcmp(argv[0],CMD_ARG_SET)==0){
		   if(argc!=2){
			  cmd_usage();
			  return;
		   }
		   uint16_t nv=atoi(argv[1]);
		   if(nv >= MIN_VALAUE && nv <= MAX_VALUE){
			   hello_control.value=nv;
			   sh_printf("successfully changed to %d\r\n",nv);
			   return;
		   }
		   sh_printf("value must be between %d and %d",MIN_VALAUE,MAX_VALUE);
		   return;
     }

     if(strcasecmp(argv[0],CMD_ARG_GET)==0){
    	sh_printf("Current value is %d\r\n",hello_control.value);
    	return;
     }

     sh_printf("unknown command: %sr\n",argv[0]);
     cmd_usage();
}




